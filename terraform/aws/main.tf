provider "aws" {
  region = "eu-west-1" 
}

resource "aws_key_pair" "example" {
  key_name   = "my-key-pair"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwEcqn41bZcOPqzT/cA7g0A/oJc5idJ/iGr4QvjpwQnGDJkDPKC+SSA5xtrzgRM4UsiznIQCSVoKtN1v/I8mFoH+cVyRY4SJxYoLEERAGrITTTDr9UVgWy+AYh0F1ZWbvYinMq34TLdOPIRcTcXDAgcu+WR5fu03Y7W5/1pqAMsy9fAe5HRe+XKuI6JOr+WKGTfPYe6i2QEBTBK4oAbvOcZTbqsO8d4GKJqN3CxX+pxC1xATsL40RFTCUa+xM/uZZZ6IyDlDw3LpgReJTFGWifjuEal5IJCOEu5maTOLa9PLI51Z5rRee4ci/rNj4FwA/nv+PCzAQXMThJPuba/ScOu6LnyfUWA2cn4fNgBQdhvgbMl1BF73TI8wf7UxeBXHa11d9OI4o5nMnifDBG48vZcVzNGETo1GaOb/b93DACKs3w5BedyrLHRVIS8nl3uBlPvywf+xHJh0pWfZgEnMfIlPggXg3YxiRis0/+DmSgZgVEWxoYWO5FOIBsCP1FJGM= neoanderson@Anderson"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Pour permettre l'accès SSH depuis n'importe où
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Pour permettre l'accès HTTP depuis n'importe où
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"  # Autoriser tout le trafic sortant
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_instance" "my_instance" {
  ami           = "ami-046a9f26a7f14326b"  # Remplacez par l'ID de l'AMI de votre choix
  instance_type = "t2.micro"     # Choisissez le type d'instance approprié

  key_name      = aws_key_pair.example.key_name
  tags = {
    Name = "MyEC2Instance"
  }
}


